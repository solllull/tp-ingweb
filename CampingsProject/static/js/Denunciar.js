function onDenunciaTerminada(datos){
    print(datos)
    alert(datos);
    window.location.href = 'tema/{{pk}}/';
}

function onClickDenunciar(e) {
    var target = $(e.target);
    print('target: '+target)
    var pk = target.data("pk-comment");
    // hacer algo con pkCosa, por ejemplo mandar request ajax de denuncia
    var motivos = document.getElementById("motivos");
    var indiceSeleccionado = motivos.selectedIndex;
    var opcionSeleccionada = motivos.options[indiceSeleccionado];
    var URL ="denuncia/{{pk}}/";
    print(pk)
    print(csrftoken)
    var data = {'motivo': opcionSeleccionada, 'pk':pk}; 
    $.post({
        headers: {"X-CSRFToken": csrftoken},   //así django acepta el POST
        url: URL,
        data: data,
    }).done(onDenunciaTerminada);
}

$('.denunciar').on('click', onClickDenunciar);


