from django.contrib import admin
from .models import ForumPost, Comentario

admin.site.register(ForumPost)
admin.site.register(Comentario)