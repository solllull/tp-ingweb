from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from ckeditor.fields import RichTextField

from sitio.models import Motivo

class ForumPost(models.Model):
    title = models.CharField(max_length=30)
    body = RichTextField(blank=True, null=True)
    #body = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title + ' | ' + str(self.author)


class Comentario(models.Model):
    author=models.ForeignKey(User, on_delete=models.CASCADE, related_name='responde')
    post = models.ForeignKey(ForumPost, on_delete=models.CASCADE, related_name='comentarios') # el related_name nos va a permitir acceder a los comentarios desde los campings
    contenido = models.CharField(max_length=500)
    #temaForo = models.ForeignKey(TemaForo, on_delete=models.CASCADE, blank=True, default='')
    fechaCreacion = models.DateTimeField(auto_now_add=True)
    liked = models.BooleanField(default=False)
    likes = models.ManyToManyField(User, related_name='forum_answers')
    denuncias = models.ManyToManyField(User, related_name='denuncia')

    def __str__(self):
        return self.contenido

    def total_likes(self):
        return self.likes.count()

    def total_denuncias(self):
        return self.denuncias.count()


CHOICES= (
('1','Contenido discriminativo'),
('2','Comentario violento'),
('3','Comentario de amenaza'),
('4','Contenido irrespetuoso'),
('5','Otro'),
)

class Denuncias(models.Model):
    comentario = models.ForeignKey(Comentario, on_delete=models.CASCADE, related_name='comentario_denunciado')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_que_denuncia')
    select = models.ForeignKey(Motivo, on_delete=models.CASCADE)