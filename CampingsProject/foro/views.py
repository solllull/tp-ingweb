from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import ForumPost, Comentario, Denuncias, CHOICES
from .forms import (ForumPostForm,
                    ComentariosForm,
                    DenunciaForm,
                    )
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.db.models import Count
from django.http import JsonResponse

from sitio.models import Motivo

def forumposts(request):
    posts = ForumPost.objects.all()
    return render(request, 'forumposts.html', {'posts':posts})

@login_required
def forumpostdetail(request, pk):
    post = ForumPost.objects.get(pk = pk)
    comentarios = Comentario.objects.filter(post = post).annotate(like_count=Count('likes')).order_by('-like_count')
    #ordenar las respuestas por cantidad de likes
    #Article.objects.annotate(like_count=Count('likes')).order_by('-like_count')
    #COMENTARIOS o sea RESPUESTAS
    if request.method == "POST":
        form = ComentariosForm(data=request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.post = post
            comentario.author = request.user
            comentario.save()
    else:
        form = ComentariosForm()

    # ver si el usuario le puso o no like a la respuesta
    for c in comentarios:
        c.total = c.likes.count()
        if c.likes.filter(id=request.user.id).exists():
            c.liked = True

    #motivos_denuncia = DenunciaForm()
    motivos_denuncia = CHOICES
    denuncias_usuario = Denuncias.objects.filter(user=request.user)
    
    comentarios_denunciados = []
    for denuncia in denuncias_usuario:
        coment = denuncia.comentario
        comentarios_denunciados.append(coment)
    return render(
        request,
        'detail.html',
        {
            'post':post,
            'comentarios': comentarios,
            'motivos': motivos_denuncia,
            'denuncias': comentarios_denunciados,
        }
    )

@login_required
def deleteanswer(request, pk):
    answer = Comentario.objects.get(pk=pk)
    answer.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def addforumpost(request):
    if request.method == "POST":
        form = ForumPostForm(request.POST)
        if form.is_valid():
            forumpost = form.save(commit=False)
            forumpost.author = request.user
            forumpost.save()
            return redirect('forumpostdetail', pk=forumpost.pk)
    else:
        form = ForumPostForm()
    return render(request, 'add_forumpost.html', {'form': form})

@login_required
def deleteforumpost(request, pk):
    # pk del forum post
    forumpost = ForumPost.objects.get(pk = pk)
    forumpost.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def likeanswer(request, pk):
    answer = get_object_or_404(Comentario, id=request.POST.get('answer_id'))
    liked = False
    if answer.likes.filter(id=request.user.id).exists():
        answer.likes.remove(request.user)
        liked = False
    else:
        answer.likes.add(request.user)
        liked = True
    return HttpResponseRedirect(reverse('forumpostdetail', args=[str(answer.post.id)]))


def denuncia_anterior(request, pk = None):
    answer = get_object_or_404(Comentario, id=request.POST.get('answer_id'))
    if request.is_ajax():
        form = DenunciaForm(data=request.POST)
        denuncia = Denuncias()
        if form.is_valid:
            
            #if answer.denuncias.filter(id=request.user.id).exists() == False:
            answer.denuncias.add(request.user)
            denuncia.comentario = answer
            denuncia.user = request.user
            #denuncia.select = data['motivo']
            print(denuncia.select)
            #denuncia.save()
            mensaje = "Denuncia correcta"
        else:
            mensaje = "form invalido"
    else:
        mensaje = "NO AJAX"
    return JsonResponse({
		'mensaje': mensaje,
		'url': reverse('forumpostdetail', args=[str(answer.post.id)]),
	})

def denuncia(request, pk):
    answer = get_object_or_404(Comentario, pk=pk)
    if request.method == 'POST': 
        form = DenunciaForm(data=request.POST)
        denuncia = Denuncias()
        if form.is_valid:
            #if answer.denuncias.filter(id=request.user.id).exists() == False:
            answer.denuncias.add(request.user)
            denuncia.comentario = answer
            denuncia.user = request.user
            motivo = Motivo.objects.get(pk=request.POST['select'])
            denuncia.select = motivo
            denuncia.save()
            return HttpResponseRedirect('/')
    else:
        form = DenunciaForm()
    motivos_denuncia = CHOICES
    return render(
        request,
        'denunciar.html',
        {
            'form':form,
            'motivos': motivos_denuncia,
            'comment': answer,
        },
    )
