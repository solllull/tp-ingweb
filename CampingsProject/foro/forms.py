from django import forms
from .models import ForumPost, Comentario, CHOICES, Denuncias

class ForumPostForm(forms.ModelForm):
    class Meta:
        model = ForumPost
        fields = ('title', 'body',)

class ComentariosForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['contenido',]
        widgets = {
            'contenido': forms.TextInput(attrs={'placeholder': 'Contenido'}),
        }


class DenunciaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(DenunciaForm, self).__init__(*args,**kwargs)
    
    class Meta:
        model = Denuncias
        fields = ['select',]