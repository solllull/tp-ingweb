from django.urls import path
from . import views

urlpatterns = [
    path('', views.forumposts, name='forumposts'),
    path('tema/<int:pk>/', views.forumpostdetail, name='forumpostdetail'),
    path('nuevotema/', views.addforumpost, name='addforumpost'),
    path('tema/<int:pk>/delete/', views.deleteforumpost, name='deleteforumpost'),
    path('respuesta/<int:pk>/delete/', views.deleteanswer, name='deleteanswer'),
    path('likeanswer/<int:pk>/', views.likeanswer, name= 'likeanswer'),
]
