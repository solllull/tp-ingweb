from django import forms
from .models import Reserva
from django.forms.widgets import DateInput

class ReservaForm(forms.ModelForm):
    class Meta:
        model = Reserva
        fields = [
            'fechaInicio',
            'fechaFin',
            'cantidadParcelas',
        ]
        widgets = {
            'fechaInicio': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
            'fechaFin': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        }
        labels = {
            'cantidadParcelas': 'Cantidad de Parcelas',
            'fechaInicio': 'Fecha de inicio de reserva',
            'fechaFin': 'Fecha de fin de reserva',
        }

    def __init__(self, *args, **kwargs):
        super(ReservaForm, self).__init__(*args, **kwargs)
        # input_formats to parse HTML5 datetime-local input to datetime field
        self.fields['fechaInicio'].input_formats = ('%Y-%m-%dT%H:%M',)
        self.fields['fechaFin'].input_formats = ('%Y-%m-%dT%H:%M',)