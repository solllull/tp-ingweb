from django.shortcuts import render, reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import render
from django.views import generic
from django.utils.safestring import mark_safe

from datetime import datetime, date
from datetime import timedelta

from django.views.generic import ListView
from .models import Reserva
from .forms import ReservaForm
from .utils import Calendar
import calendar

from sitio.models import Camping
from django.contrib import messages

# Create your views here.
@login_required
def reservar(request, pk):
	next = request.POST.get('next', '/')
	pk_g = pk
	def get_object(self):
			return Reserva.objects.get(pk=self.request.GET.get('pk')) # or request.POST
	camping = Camping.objects.get(pk=pk)
	reservas = Reserva.objects.all()
	#dias_ya_reservados = False
	if request.POST:
		form = ReservaForm(data=request.POST)
		if form.is_valid():
			reserva = form.save(commit=False)
			#for r in reservas:
				#if (r.fechaInicio < reserva.fechaInicio or r.fechaFin > reserva.fechaFin) and (r.rechazado==False and r.confirmado==True):
					#dias_ya_reservados = True
			#if dias_ya_reservados:
			#	pass
				#form.add_error('fechaInicio', "Días ya reservados")
			#else:
			reserva.usuario = request.user
			reserva.camping =camping
			reserva.save()
			return HttpResponseRedirect(next)
	else:
		form = ReservaForm()
	return render(
		request,
		'reservar_calendario.html',
		{'form':form,
		'camping':camping,
		'reservas': reservas,
		'pk': camping.pk}
	)

@login_required
def mostrar_confirmar_reserva(request, pk):
	reserva= Reserva.objects.get(pk=pk)
	usuario = User.objects.get(pk=reserva.usuario.pk)
	return render(
		request,
		'confirmar_reserva.html',
		{
			'reserva': reserva,
			'usuario': usuario,
		}
	)


@login_required
def confirmar_reserva(request):
	if request.POST:
		pk = request.POST['pk']
		try:
			reserva = Reserva.objects.get(pk=pk)
			reserva.confirmado = True
			reserva.save()
			messages.success(request, 'Reserva confirmada con exito.')
			mensaje = 'Confirmado correctamente'
			HttpResponseRedirect('/')
		except Reserva.DoesNotExist:
			mensaje = 'Reserva no encontrada'
	else:
		mensaje= 'NO POST'

	return JsonResponse({
		'mensaje': mensaje,
		'url': '/',
	})

@login_required
def rechazar_reserva(request):
	if request.POST:
		pk = request.POST['pk']
		try:
			reserva = Reserva.objects.get(pk=pk)
			reserva.rechazado = True
			reserva.save()
			messages.success(request, 'Reserva rechazada.')
			mensaje = 'Rechazado correctamente'
			HttpResponseRedirect('/')
		except Reserva.DoesNotExist:
			mensaje = 'Reserva no encontrada'
	else:
		mensaje= 'NO POST'

	return JsonResponse({
		'mensaje': mensaje,
		'url': '/',
	})

def mostrar_reservas(request,pk):
	camping = Camping.objects.get(pk=pk)
	reservas = Reserva.objects.filter(camping=camping)

	return render(request, 'mostrar_reservas.html', {'reservas': reservas,})

def mis_reservas(request):
	reservas = []
	try:
		reservas = Reserva.objects.filter(usuario=request.user)
		for reserva in reservas:
			reserva.noti_leida = True
			reserva.save()
	except:
		pass

	return render(request, 'mis_reservas.html', {'reservas': reservas,})

@login_required()
def reservas_details(request, event_id):
	event = Reserva.objects.get(id=event_id)
	context = {
		'reserva': event,
	}
	return render(request, 'detalle_reservas.html', context)

class CalendarView(LoginRequiredMixin, ListView):
	template_name = 'calendar.html'

	def get_queryset(self, **kwargs):
		parametro = self.kwargs['pk']
		camping = Camping.objects.get(pk=parametro)
		reservas = Reserva.objects.all()
		for reserva in reservas:
			print(reserva.camping.nombre)
		print(parametro)
		print(camping.nombre)
		return Reserva.objects.filter(confirmado=True)

	def get_context_data(self, **kwargs):
		parametro = self.kwargs['pk']
		camping = Camping.objects.get(pk=parametro)
		nombre = camping.nombre

		context = super().get_context_data(**kwargs)
		d = get_date(self.request.GET.get('month', None))
		cal = Calendar(d.year, d.month)
		html_cal = cal.formatmonth(nombre, withyear=True)
		context['calendar'] = mark_safe(html_cal)
		context['prev_month'] = prev_month(d, nombre)
		context['next_month'] = next_month(d)
		context['pk'] = self.kwargs['pk']
		return context

def get_date(req_day):
	if req_day:
		year, month = (int(x) for x in req_day.split('-'))
		return date(year, month, day=1)
	return datetime.today()

def prev_month(d, nombre):
	first = d.replace(day=1)
	prev_month = first - timedelta(days=1)
	month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
	return month

def next_month(d):
	days_in_month = calendar.monthrange(d.year, d.month)[1]
	last = d.replace(day=days_in_month)
	next_month = last + timedelta(days=1)
	month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
	return month