from datetime import datetime, timedelta
from calendar import HTMLCalendar
from .models import Reserva
from CampingsProject.helper import get_current_user
from sitio.models import Camping

class Calendar(HTMLCalendar):
	def __init__(self, year=None, month=None):
		self.year = year
		self.month = month
		super(Calendar, self).__init__()
	# filter events by day
	def formatday(self, day, reservas):
		reservas_per_day = reservas.filter(fechaInicio__day=day)
		d = ''

		for reserva in reservas_per_day:
			d += f'<li> {reserva.get_html_url} </li>'

		if day != 0:
			return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
		return '<td></td>'

	# formats a week as a tr
	def formatweek(self, theweek, reservas):
		week = ''
		for d, weekday in theweek:
			week += self.formatday(d, reservas)
		return f'<tr> {week} </tr>'

	# formats a month as a table
	# filter events by year and month
	def formatmonth(self, nombre, withyear=True):
		reservas = Reserva.objects.filter(fechaInicio__year=self.year, fechaInicio__month=self.month, camping__nombre=nombre, confirmado=True)

		cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
		cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
		cal += f'{self.formatweekheader()}\n'
		for week in self.monthdays2calendar(self.year, self.month):
			cal += f'{self.formatweek(week, reservas)}\n'
		return cal