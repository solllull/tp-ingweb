from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.db import models

# Create your models here.

from sitio.models import Camping
from django.core.exceptions import ValidationError

class Reserva(models.Model):
	camping = models.ForeignKey(Camping, on_delete=models.CASCADE)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)
	fechaInicio=models.DateTimeField()
	fechaFin=models.DateTimeField()
	cantidadParcelas=models.PositiveIntegerField(default=0)
	confirmado = models.BooleanField(default=False)
	rechazado = models.BooleanField(default=False)
	noti_leida = models.BooleanField(default=False)

	def check_overlap(self, fixed_start, fixed_end, new_start, new_end):
		overlap = False
		if new_start == fixed_end or new_end == fixed_start:    #edge case
			overlap = False
		elif (new_start >= fixed_start and new_start <= fixed_end) or (new_end >= fixed_start and new_end <= fixed_end): #innner limits
			overlap = True
		elif new_start <= fixed_start and new_end >= fixed_end: #outter limits
			overlap = True

		return overlap

	def clean(self):
		if self.fechaFin <= self.fechaInicio:
			raise ValidationError('La fecha de fin no puede suceder antes que la de inicio.')

		# events = Reserva.objects.filter(confirmado=True)
		# if events.exists():
		# 	for event in events:
		# 		if self.check_overlap(event.fechaInicio, event.fechaFin, self.fechaInicio, self.fechaFin):
		# 			raise ValidationError(
		# 				'Hay una superposición con otra reserva: El día ' + str(event.fechaInicio.day) + ', ' + str(
		# 					event.fechaInicio) + '-' + str(event.fechaFin))

	def get_absolute_url(self):
		return reverse('/reservas/'+id+'/details/', args=(self.id,))

	@property
	def get_html_url(self):
		id = str(self.id)
		url = '/reservas/'+id+'/details/'
		return f'<a href="{url}"> Reserva hecha por {self.usuario} </a>'