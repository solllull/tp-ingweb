from .views import reservar, confirmar_reserva, mostrar_confirmar_reserva, rechazar_reserva, mostrar_reservas, reservas_details
from django.urls import path
from django.conf.urls import url

from .views import CalendarView

app_name = 'reservas'
urlpatterns = [
    path('calendar/<int:pk>/reservar/', reservar, name='reservar'),
    path('<int:pk>/reservar/', reservar, name='reservar_dos'),
    path('confirmar/', confirmar_reserva, name='confirmar'),
    path('rechazar/', rechazar_reserva, name='rechazar'),
    path('confirmar/<int:pk>/', mostrar_confirmar_reserva, name='mostrar-confirmacion'),
    path('mis-reservas/<int:pk>/', mostrar_reservas, name='mis-reservas'),

    path('calendar/<int:pk>/', CalendarView.as_view(), name='calendar'),
    path('<int:event_id>/details/', reservas_details, name='reservas-details'),
]

"""
    path('detallecamping/<int:pk>/calendar/', views.CalendarView.as_view(), name='calendar'),
    path('<int:pk>/event/new/', views.create_event, name='event_new'),
    path('event/edit/<int:pk>/', views.EventEdit.as_view(), name='event_edit'),
    path('event/<int:event_id>/details/', views.event_details, name='event-detail'),
    path('add_eventmember/<int:event_id>', views.add_eventmember, name='add_eventmember'),
    path('event/<int:pk>/remove', views.EventMemberDeleteView.as_view(), name="remove_event"),
    """