from django.contrib import admin
from django.urls import path, include

from .views import (home,
                    iniciar_sesion,
                    registrarse,
                    cerrar_sesion,
                    reservaciones,
                    detalle_camping,
                    campings,
                    listar,
                    agregar_editar,
                    eliminar_banner,
                    activate,
                    borrarcamping,
                    abmcamping,
                    eliminar_comentario,
                    autocomplete,
                    denuncias,
                    descartardenuncias,
                    autocompleteForumpost
                    )

from django.contrib.auth import views as auth_views
from django.conf.urls import url
from foro.views import forumpostdetail, deleteforumpost
from reservas.views import mis_reservas
from foro import views

app_name = 'sitio'
urlpatterns = [
    path('', home, name='home'),
    path('accounts/login/', iniciar_sesion, name='iniciosesion'),
    path('registrarse/', registrarse, name='registrarse'),
    path('cerrarsesion/', cerrar_sesion, name='cerrarsesion'),

    path('reservaciones/', reservaciones, name='reservaciones'),
    path('campings/<int:pk>/', campings, name = 'campings'),
    # path('campings/', campings, name='campings'),
    path('detallecamping/<int:pk>/', detalle_camping, name='detalle_camping'),
    path('banners/', listar, name='lista_banners'),
    path('banner/', agregar_editar, name='banner'),
    path('banner/<int:pk>/', agregar_editar, name='banner_detalle'),
    path('banner/<int:pk>/delete/', eliminar_banner, name='eliminar'),
    # path('nuevocamping/', nuevocamping, name='nuevocamping'),
    path('abmcamping/', abmcamping, name='abmcamping'),
    #path('modificarcamping/<int:pk>/', modificarcamping, name='modificarcamping'),
    path('borrarcamping/<int:pk>/', borrarcamping, name='borrarcamping'),
    path('activate/<uidb64>/<token>/', activate, name='activate'),
    #comentarios
    path('comentario/<int:pk>/delete/', eliminar_comentario),
    path('reservas/', include('reservas.urls')),

    path('misreservas/', mis_reservas, name='misreservas'),

    path('search/', include('haystack.urls')),
    path('foro/search/', include('haystack.urls')),

    path('forumpostdetail/<int:pk>/', forumpostdetail, name='forumpostdetail'),
    path('deleteforumpost/<int:pk>/', deleteforumpost, name='deleteforumpost'),

    url(r'^search/autocomplete',autocomplete, name='autocomplete'),
    url(r'^foro/search/autocomplete',autocompleteForumpost, name='autocomplete'),

    path('denuncias/', denuncias, name='denuncias'),

    path('descartardenuncias/<int:pk>/', descartardenuncias, name= 'descartardenuncias'),

    path('denuncia/<int:pk>/', views.denuncia, name= 'denuncia'),
]