from haystack import indexes
from .models import Camping
from foro.models import ForumPost

class campingIndex(indexes.SearchIndex, indexes.Indexable):
    #Lo que quiero que me devuelva
    text= indexes.CharField(document=True, use_template=True)
    nombre=indexes.CharField(model_attr='nombre')
    descripcion=indexes.CharField(model_attr='descripcion')

    content_auto = indexes.EdgeNgramField(model_attr='nombre')

    def get_model(self):
        return Camping

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class posteosIndex(indexes.SearchIndex, indexes.Indexable):
    #Lo que quiero que me devuelva
    text= indexes.CharField(document=True, use_template=True)
    title=indexes.CharField(model_attr='title')
    body=indexes.CharField(model_attr='body')
    author=indexes.CharField(model_attr='author')

    content_auto = indexes.EdgeNgramField(model_attr='title')

    def get_model(self):
        return ForumPost

    def index_queryset(self, using=None):
        return self.get_model().objects.all()