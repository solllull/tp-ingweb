from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth import login as do_login
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.template.loader import render_to_string
from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.core import mail
from django.utils import timezone
from django.db.models import Count

from .forms import (InicioSesionForm,
                    Registrarse,
                    PasswordResetForm,
                    ComentariosForm,
                    BannerForm,
                    CampingForm,
                    )
from .models import (Camping,
                    Comentario,
                    Banner,
                    Zona,
                    Localidad,
                    Motivo,
                    )
from star_ratings.models import Foo

from reservas.models import Reserva
from foro import models

UserModel = get_user_model()

from django.contrib import messages
from haystack.query import SearchQuerySet
import json

# Create your views here.

def autocomplete(request):
    texto = request.GET.get('q', '')
    print(texto)
    sqs = SearchQuerySet().autocomplete(content_auto=request.GET.get('q', ''))[:5]
    suggestions = [result.nombre for result in sqs]
    suggestions=list(set(suggestions))
    the_data = json.dumps({
        'results': suggestions,
        'busqueda': texto,
    })
    return HttpResponse(the_data, content_type='application/json')

def autocompleteForumpost(request):
    sqs = SearchQuerySet().autocomplete(content_auto=request.GET.get('q', ''))[:5]
    suggestions = [result.title for result in sqs]
    suggestions=list(set(suggestions))
    the_data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(the_data, content_type='application/json')

def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.signup_confirmation = True
        user.save()
        messages.success(request, 'Usuario registrado con éxito. Inicie sesion.')
        #login(request, user)
        return HttpResponseRedirect('/')
    else:
        messages.warning(request, 'Link de activacion inválido.')
        return HttpResponseRedirect('/')
    return render(request, 'home.html', {})

def registrarse(request):
    args = {}
    if request.method == 'POST': #POST es como para leer y escribir, GET para leer
        form = Registrarse(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            message = render_to_string('registration/activation_request.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user),
            })
            mail_subject = 'Activación de cuenta en Vivis en Carpa'
            to_email = form.cleaned_data.get('email')
            mail.EmailMessage(mail_subject, message, to=[to_email]).send()
            messages.success(request, 'El link para activar tu cuenta fue enviado. Por favor verifica tu mail.')
            return HttpResponseRedirect('/')
    else:
        form = Registrarse()
    args['form'] = form
    return render(request, 'registration/registrarse.html', args)


def home(request):
    return render(
        request,
        'home.html',
        {'zonas': Zona.objects.all(),
        'localidades': Localidad.objects.all(),
        }
    )

def listar(request):
    camping = Camping.objects.filter(usuarioAdmin=request.user).first()
    banners = Banner.objects.filter(camping=camping)
    return render(request, 'banners/lista.html', {'banners': banners,
                                                'camping': camping,
                                                })

# Crear y editar banner
@login_required
def agregar_editar(request, pk=None):
    try:
        elemento = Banner.objects.get(pk=pk)
    except Banner.DoesNotExist:
        elemento = None
    if request.POST:
        form = BannerForm(data=request.POST, files=request.FILES, instance=elemento)
        if form.is_valid():
            # validar si imagen es jpg y no supera determinado tamaÃ±o en alto y ancho
            banner = form.save(commit=False)
            extension = banner.extension()
            guardar = False
            if extension == '.jpg':
                guardar = True
                if banner.imagen.width > 1920:
                    guardar = False
                    form.add_error('imagen', "Ancho mÃ¡ximo 1920 px")
                if banner.imagen.height > 1000:
                    guardar = False
                    form.add_error(
                        'imagen',
                        "Su imagen tiene %s px de alto. El alto mÃ¡ximo es de 500 px" % banner.imagen.height
                    )
            else:
                form.add_error('imagen', "ExtensiÃ³n invÃ¡lida. Solo JPG")
            if guardar:
                banner.usuario = request.user
                banner.camping = Camping.objects.get(usuarioAdmin=request.user)
                banner.save()
                return HttpResponseRedirect('/banners/')

    else:
        form = BannerForm(instance=elemento)

    return render(
        request,
        "banners/agregar_editar.html",
        {
            'banners': Banner.objects.all(),
            'form': form,
            'elemento': elemento,
        }
    )

# Eliminar banner
@login_required
def eliminar_banner(request, pk):
    instancia= Banner.objects.get(pk=pk)
    instancia.delete()
    return HttpResponseRedirect('/banners/')


def reservaciones(request):
    return render(request, 'reservaciones.html', {})

def campings(request, pk):
    loc = Localidad.objects.get(pk=pk)
    campings = Camping.objects.filter(localidad=loc)
    return render(request, 'mostrar_campings.html', {'campings':campings})

def iniciar_sesion(request):
    args = {}
    valuenext= request.POST.get('next')
    if request.method == "POST":
        form= InicioSesionForm(request.POST)
        if form.is_valid():
            username = request.POST['nombreUsuario']
            password = request.POST['contraseña']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    print(valuenext)
                    if valuenext:
                        return HttpResponseRedirect(valuenext)
                    else:
                        return HttpResponseRedirect('/')
                else:
                    return redirect('login')
    else:
        form= InicioSesionForm()
        print(valuenext)

    args['form'] = form

    return render(request, 'accounts/login.html', args)

def cerrar_sesion(request):
    logout(request)
    return HttpResponseRedirect('/')

    return render(request, 'home.html', {})


def detalle_camping(request, pk):
    camping = Camping.objects.get(pk=pk)
    comentarios = Comentario.objects.filter(camping = camping)
    servicios = camping.servicios.all()
    
    #COMENTARIOS
    if request.method == "POST":
        form = ComentariosForm(data=request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.camping = camping
            comentario.usuarioAutor = request.user
            #comentario.rating = form.cleaned_data['rating']
            comentario.save()
    else:
        form = ComentariosForm()
    banners = Banner.objects.filter(fecha_inicio__lt=timezone.now(),
                                    fecha_fin__gte=timezone.now(),
                                    camping=camping)
    return render(
        request, 
        'home_copy.html', {
        'camping': camping,
        'comentarios': comentarios,
        'servicios': servicios,
        'form': form,
        'banners': banners,
        'object': (Camping.objects.get(pk=pk)).estrellas,
        }
    )


@login_required
def eliminar_comentario(request, pk):
    instancia = Comentario.objects.get(pk=pk)
    instancia.delete()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def abmcamping(request):
    try:
        camping = Camping.objects.filter(usuarioAdmin = request.user).first()
    except Banner.DoesNotExist:
        camping = None

    if camping == None:
        form = CampingForm()
        if request.method == 'POST':
            form = CampingForm(data=request.POST, files=request.FILES)
            if form.is_valid():
                c = form.save(commit=False)
                c.usuarioAdmin = request.user
                c.estado = 'Activo'
                obj, created = Foo.objects.get_or_create(bar='Rating de '+c.nombre)
                c.estrellas = obj
                c.save()
                form.save_m2m()
                return HttpResponseRedirect('/')

        context = {'form':form, 'camping':camping}
        return render(request, 'nuevocamping.html', context)
    else:
        # camping = Camping.objects.get(pk = pk)
        form = CampingForm(instance=camping) # para que traiga los datos del camping a editar

        if request.method == 'POST':
            print(request.FILES)
            form = CampingForm(data=request.POST, files=request.FILES, instance=camping)
            if form.is_valid():
                c = form.save(commit=False)
                c.usuarioAdmin = request.user
                c.estado = 'Activo'
                obj, created = Foo.objects.get_or_create(bar='Rating de '+c.nombre)
                print(obj)
                c.estrellas = obj
                c.save()
                form.save_m2m()
                return HttpResponseRedirect('/')

        context = {'form':form, 'camping':camping}
        return render(request, 'nuevocamping.html', context)


@login_required
def borrarcamping(request, pk):
    camping = Camping.objects.get(pk = pk)
    camping.delete()
    return HttpResponseRedirect('/abmcamping/')
    return render(request, 'nuevocamping.html', context={'camping':None,})


@login_required
def denuncias(request):
    # obtener los comentarios con más de 5 denuncias 
    comentarios_denunciados = models.Comentario.objects.annotate(total_denuncias=Count('denuncias')).filter(total_denuncias__gte=5)
    
    motivos = [] # contendrá por cada comentario [comentario, [motivos por los que fue denunciado]]

    for comment in comentarios_denunciados:
        sublista = [[], []]
        sublista[0] = comment
        # obtener denuncias
        denuncias = models.Denuncias.objects.filter(comentario=comment)
        for d in denuncias:
            motivo = Motivo.objects.get(descripcion=d.select)
            if motivo not in sublista[1]:
                sublista[1].append(motivo)
        motivos.append(sublista)
    
    return render(request, 'comentarios_denunciados.html', {'comentarios':comentarios_denunciados,
    'motivos': motivos })


@login_required
def descartardenuncias(request, pk):
    comentario = models.Comentario.objects.get(pk=pk)
    # eliminar las denuncias hechas a ese comentario/respuesta
    comentario.denuncias.clear()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
