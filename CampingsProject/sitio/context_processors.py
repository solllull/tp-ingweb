from reservas.models import Reserva, Camping
from django.shortcuts import get_object_or_404


def agregar_info_notificaciones(request):
    reservas_list = []
    camping = ""
    cantidad = ""
    mis_reservas_list = []
    mis_reservas = []
    if not request.user.is_anonymous:
        try:
            #-------------------para el dueño del camping-------------------------#
            camping = Camping.objects.get(usuarioAdmin=request.user)
            reservas = Reserva.objects.filter(camping=camping)
            for reserva in reservas:
                if (reserva.confirmado == False and reserva.rechazado == False):
                    reservas_list.append(reserva)
            cantidad = len(reservas_list)
            #------------------------para el usuario------------------------------#
            mis_reservas = Reserva.objects.filter(usuario=request.user)
            for mi_reserva in mis_reservas:
                if (mi_reserva.noti_leida == False and (mi_reserva.confirmado==True or mi_reserva.rechazado==True)):
                    mis_reservas_list.append(mi_reserva)
            cantidad+= len(mis_reservas_list)
        except Camping.DoesNotExist:
            camping = ""
    return {
        'reservas': reservas_list,
        'cantidad': cantidad,
        'camping': camping,
        'mis_reservas': mis_reservas_list,
        'cant_mis_reservas': len(mis_reservas)
    }

