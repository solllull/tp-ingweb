import os

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.forms.models import ModelForm
from star_ratings.models import Foo

# Create your models here.

# class PerfilUsuario(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE) #extiende atributos de User https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone
#     estado = models.CharField(max_length=20)
#     signup_confirmation = models.BooleanField(default=False)
#     def __str__(self):
#         return self.user.username

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()

class Zona(models.Model):
    nombre=models.CharField(max_length=30)

    def __str__(self):
        return self.nombre

class Localidad(models.Model):
    nombre=models.CharField(max_length=50)
    zona=models.ForeignKey(Zona, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class Servicio(models.Model):
    descripcion=models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion


class Camping(models.Model):
    nombre = models.CharField(max_length=50)
    usuarioAdmin=models.OneToOneField(User, on_delete=models.CASCADE)
    estado=models.CharField(max_length=20)
    descripcion=models.TextField()
    localidad=models.ForeignKey(Localidad, on_delete=models.CASCADE)
    direccion = models.CharField(max_length=50, null=True)
    pagina = models.CharField(max_length=50, null=True)
    telefono = models.CharField(max_length=12, null=True)
    linkGoogleMaps = models.TextField(blank=True, default="")
    servicios = models.ManyToManyField(Servicio, blank=True)
    foto_perfil = models.ImageField(upload_to='static/img/foto_perfil', blank=True, default="")
    estrellas = models.ForeignKey(Foo, on_delete=models.CASCADE, blank=True, db_constraint=False)

    def __str__(self):
        return self.nombre


class Banner(models.Model):
    titulo = models.CharField(max_length=100)
    usuario = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    fecha_creado = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    imagen = models.ImageField(upload_to='static/img/banners')
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    camping = models.ForeignKey(Camping, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo

    def extension(self):
        name, extension = os.path.splitext(self.imagen.name)
        return extension



# class Servicio_Camping(models.Model):
#     camping=models.ForeignKey(Camping, on_delete=models.CASCADE)
#     servicio=models.ForeignKey(Servicio, on_delete=models.CASCADE)


class TemaForo(models.Model):
    titulo = models.CharField(max_length=30)
    contenido=models.TextField()
    usuarioAutor = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo

STARS = [                                    
            ('1', 'Una estrella'),                        
            ('2', 'Dos estrellas'),                 
            ('3', 'Tres estrellas'),  
            ('4', 'Cuatro estrellas'),         
            ('5', 'Cinco estrellas'),                          
        ] 

class Comentario(models.Model):
    usuarioAutor=models.ForeignKey(User, on_delete=models.CASCADE)
    camping = models.ForeignKey(Camping, on_delete=models.CASCADE, related_name='comentarios') # el related_name nos va a permitir acceder a los comentarios desde los campings
    contenido = models.CharField(max_length=10000)
    #temaForo = models.ForeignKey(TemaForo, on_delete=models.CASCADE, blank=True, default='')
    fechaCreacion = models.DateTimeField(auto_now_add=True)
             
    # rating = models.CharField(choices=STARS, max_length=2, blank=True, null=True)

    def __str__(self):
        return self.contenido


# class ComentarioForm(ModelForm):
#     class Meta:
#         model = Comentario
#         fields = ['contenido', 'rating']


class Motivo(models.Model):
    descripcion=models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

class Denuncia(models.Model):
    comentario = models.ForeignKey(Comentario, on_delete=models.CASCADE)
    motivo = models.ForeignKey(Motivo, on_delete=models.CASCADE)
    usuarioDenunciante=models.ForeignKey(User, on_delete=models.CASCADE, related_name='denunciante')
    usuarioAcusado=models.ForeignKey(User, on_delete=models.CASCADE, related_name='acusado')

class Atractivo(models.Model):
    descripcion=models.TextField()
    localidad=models.ForeignKey(Localidad, on_delete=models.CASCADE)
    usuarioAutor=models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.descripcion


