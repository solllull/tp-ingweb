from django.contrib import admin
from .models import (
    Camping,
    Comentario,
    Localidad,
    Zona,
    Banner,
    Servicio,
    Motivo,
    )

# Register your models here.
admin.site.register(Zona)
admin.site.register(Localidad)
admin.site.register(Camping)
admin.site.register(Comentario)
admin.site.register(Banner)
admin.site.register(Servicio)
admin.site.register(Motivo)