# Generated by Django 3.1 on 2020-09-08 17:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitio', '0021_remove_comentario_rating'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='imagen',
            field=models.ImageField(upload_to='static/img/banners'),
        ),
    ]
