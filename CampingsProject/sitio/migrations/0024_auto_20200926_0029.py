# Generated by Django 3.1 on 2020-09-26 03:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('star_ratings', '0001_initial_manual'),
        ('sitio', '0023_camping_estrellas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='camping',
            name='estrellas',
            field=models.ForeignKey(blank=True, db_constraint=False, on_delete=django.db.models.deletion.CASCADE, to='star_ratings.foo'),
        ),
    ]
