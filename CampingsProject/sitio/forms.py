from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)

from .models import (
    Camping,
    Comentario,
    Banner,
    Servicio,
    #ComentarioForm,
    )

from django.forms.widgets import CheckboxSelectMultiple


class DateInput(forms.DateInput):
    input_type = 'date'

class BannerForm(forms.ModelForm):

    class Meta:
        model = Banner
        fields = [
            'titulo',
            'imagen',
            'fecha_inicio',
            'fecha_fin',
        ]
        widgets = {
            'titulo': forms.TextInput(attrs={'placeholder': 'Titulo del banner'}),
            'fecha_inicio': DateInput,
            'fecha_fin': DateInput,
        }
        labels = {
            'titulo': 'Titulo Imagen',
            'fecha_inicio': 'Fecha de inicio de publicaciÃ³n',
            'fecha_fin': 'Fecha de fin de publicaciÃ³n',
        }

from django.contrib.auth.hashers import check_password

class InicioSesionForm(forms.Form):
    nombreUsuario = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Usuario'}), required=True)
    contraseña = forms.CharField(widget=forms.PasswordInput, required=True)

    def clean_contraseña(self):
        username = self.cleaned_data.get('nombreUsuario')
        password = self.cleaned_data.get('contraseña')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError("Por favor, ingrese el Usuario y la Contraseña correctos.")
        return password

    def clean_nombreUsuario(self):
        username = self.cleaned_data['nombreUsuario']
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError("El nombre de Usuario ingresado no existe.")
        return username

    def login(self, request):
        nombreUsuario = self.cleaned_data.get('username')
        contraseña = self.cleaned_data.get('password')
        user = authenticate(username=nombreUsuario, password=contraseña)
        return user

from django.contrib.auth.hashers import check_password

class Registrarse(UserCreationForm):
    email = forms.EmailField(max_length=100)

    password1 = forms.CharField(
        label='Contraseña',
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
    )
    password2 = forms.CharField(
        label='Confirmar contraseña',
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False,
    )

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 != password2:
            raise forms.ValidationError("Las contraseñas no coinciden.")

        if len(password1) < 8:
            raise forms.ValidationError('Las contraseñas no pueden tener menos de 8 caracteres.')

        return password2

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.exclude().filter(username=username).exists():
            raise forms.ValidationError('El Nombre elegido ya existe.')

        return username

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.exclude().filter(email=email).exists():
            raise forms.ValidationError('El Email elegido ya esta en uso.')
        return email

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'password1',
            'password2',
        )
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': ''}),
            'email': forms.EmailInput(attrs={'placeholder': ''}),
        }
        labels = {
            'username': 'Nombre de usuario',
            'email': 'Correo electrÃ³nico',
        }
        help_texts = {
            'username': 'Ingrese 8 caracteres',
            'password1': ('Su contraseÃ±a no puede ser muy similar a su otra informaciÃ³n personal.',
                        'Su contraseÃ±a debe contener al menos 8 caracteres.',
                        'Su contraseÃ±a no puede ser una contraseÃ±a de uso comÃºn.',
                        'Tu contraseÃ±a no puede ser completamente numÃ©rica.',)
        }
        error_messages = {
            'username': {
                'max_length': 'Nombre de usuario demasiado largo.',
            },
        }

class PasswordResetForm(auth_views.PasswordResetForm):
    email = forms.EmailField(required=True)
    #class Meta:
    #    model

# STARS = [
#             ('1', 'Una estrella'),
#             ('2', 'Dos estrellas'),
#             ('3', 'Tres estrellas'),
#             ('4', 'Cuatro estrellas'),
#             ('5', 'Cinco estrellas'),
#         ]


class ComentariosForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['contenido',]
        widgets = {
            'contenido': forms.TextInput(attrs={'placeholder': 'Contenido'}),
        }

    


class CampingForm(forms.ModelForm):  
    class Meta:
        model = Camping
        fields = ('nombre', 'descripcion', 'foto_perfil', 'localidad', 'direccion', 'pagina', 'telefono',
                    'linkGoogleMaps', 'servicios')
                    
        #exclude = ['usuarioAdmin', 'estado']
    #servicios = forms.ModelMultipleChoiceField(queryset=Servicio.objects.all(), required=True)

    def __init__(self, *args, **kwargs):
            
            super(CampingForm, self).__init__(*args, **kwargs)
            
            self.fields["servicios"].widget = CheckboxSelectMultiple()
            self.fields["servicios"].queryset = Servicio.objects.all()

