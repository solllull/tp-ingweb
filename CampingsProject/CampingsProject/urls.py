"""CampingsProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls), # url por defecto para administrar el sitio
    path('', include('sitio.urls')), # incluye las urls de la app pagina
    path('foro/', include('foro.urls')),
    path('reservas/', include('reservas.urls')),
    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'), #vista que te pide el correo
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'), #avisa que te mandan correo con un link
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'), #vista de ingresar contraseña nueva
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'), #vista de contraseña cambiada correctamente
    path('ratings/', include('star_ratings.urls', namespace='ratings')),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
