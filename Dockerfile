# qué vamos a usar
FROM python:3.8.5

# cambios que le hago arriba
RUN mkdir /app_grupo6
WORKDIR /app_grupo6

COPY . /app_grupo6

# instalar todos los reqs
RUN pip install -r requirements.txt

#variable de entorno
ENV INDOCKER=True

RUN mkdir /data

CMD ["python", "CampingsProject/manage.py", "runserver", "0.0.0.0:8000"]